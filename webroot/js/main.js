$(window).load(async function () {

    await loadMonthlyAsync();
});

async function loadMonthlyAsync() {

    try {
        // DBからカレンダーデータを取得
        data = await fetchMonthlyAsync();

        $('#mycalendar').monthly({
            weekStart: 'Sun',
            mode: 'event',
            stylePast: false, // 過去日に斜線
            dataType: 'json',
            events: data,
            eventList: false,
        });

        // テキストにマーカー線を引く
        $(".marker-animation").each(function () {
            $(this).addClass('active');
        });
    }
    catch (error) {
        console.table(error);
    }
}

/** DBから予約状況の一覧を取得します。 */
async function fetchMonthlyAsync() {

    return await $.ajax({
        url: 'https://kis-group.work/aircons/getMonthly',
        type: 'GET',
        dataType: 'json',
    });
}
